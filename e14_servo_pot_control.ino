#include <Bounce.h>

#include <Servo.h> 
Servo myservo; // create servo object to control a servo 
 
int potpin = 0;
int val; 

int servoPosition = 0; //Old val1, used to store servo position. Allows use by whole program
 
// set pin numbers:
const int buttonPin = 10; // the number of the pushbutton pin
const int buttonPin2 = 8;
const int buttonPin3 = 4;
const int buttonPin4 = 2;

long debounceDelay = 50;    // the debounce time; increase if the output flickers

//Debounce objects
// Instantiate a Bounce object with a 5 millisecond debounce time
Bounce bouncer1 = Bounce(buttonPin, debounceDelay);
Bounce bouncer2 = Bounce(buttonPin2, debounceDelay);
Bounce bouncer3 = Bounce(buttonPin3, debounceDelay);
Bounce bouncer4 = Bounce(buttonPin4, debounceDelay);
 
void setup()
{
      myservo.attach(12); // attaches the servo on pin 12 to the servo object 
 
     // initialize the pushbutton pin as an input:
     pinMode(buttonPin, INPUT); 
     pinMode(buttonPin2, INPUT); 
     pinMode(buttonPin3, INPUT); 
     pinMode(buttonPin4, INPUT);
}
 
void loop()
{ 
     val = analogRead(potpin); //takes reading from pot current position 
     val = map(val, 0, 1083, 0, 180); //maps pot inputs to servo output
     myservo.write(val); //writes current position to servo to move it
 
     //Update debounce tool
     bouncer1.update();
     bouncer2.update();
     
     //Do not need to update these here are they are not used
     //bouncer3.update();
     //bouncer4.update();
     
     
     if (bouncer1.read() == HIGH)
     {
          serloop1(); //Enters button control loop
     }

     if (bouncer2.read() == HIGH)
     {
          serloop1(); //Enters button control loop
     }
}
 
 
/**
 * If button control is enabled, loop and handle control buttons
 * If exit buttons (To return to pot control) are pressed, exit loop and return
 * to pot control
 */
void serloop1()
{
     servoPosition = myservo.read(); //reads current servo location

     int btnControlEnabled = 1; //If button control is enabled this equals 1, else it equals 0

     while(btnControlEnabled == 1)
     {
       //Update all debounce tools
       bouncer1.update();
       bouncer2.update();
       bouncer3.update();
       bouncer4.update();
       
       if (bouncer1.read() == HIGH)
       {
          myservo.write(servoPosition + 2); //SUBTRACT 2 degrees to servo position for increased motor rpm
          servoPosition = myservo.read(); //Read new servo position
          delay(100); //allows time for switch ro reset
       }
       //If first button not pressed, check the next...
       else if (bouncer2.read() == HIGH)
       {
          myservo.write(servoPosition - 2); //ADDS 2 degrees to servo position for decreased motor rpm
          servoPosition = myservo.read(); //Read new servo position
          delay(100); //allows time for switch ro reset
       }
       else if (bouncer3.read() == HIGH)
       {
          btnControlEnabled = 0; //Set loop exit condition
       }
       else if (bouncer4.read() == HIGH)
       {
          btnControlEnabled = 0; //Set loop exit condition
       }
       //If nothing pressed...
       else
       {
         //...do nothing at all, go back to start of loop
       }
     }
}
